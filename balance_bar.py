#! /usr/bin/env python3

# Character Encoding: UTF-8

#system includes
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

#user includes
from slickrpc import Proxy
from slickrpc import exc

class BalanceBar(QObject):

    def __init__(self, parent):
        super(QObject, self).__init__()
        self.parent = parent

        self.baseWidget = QWidget()

        self.balanceLabel = QLabel("---")
        self.stakingLabel = QLabel("---")
        self.unconfirmedLabel = QLabel("---")
        self.totalLabel = QLabel("---")

        self.balanceBarLayout = QGridLayout()
        self.balanceBarLayout.addWidget(self.balanceWidget(), 0, 0)
        self.balanceBarLayout.addWidget(self.stakingWidget(), 0, 1)
        self.balanceBarLayout.addWidget(self.unconfirmedWidget(), 0, 2)
        self.balanceBarLayout.addWidget(self.totalWidget(), 0, 3)
        self.balanceBarLayout.addItem(self.balanceSpacerItem(), 0, 4)
        self.baseWidget.setLayout(self.balanceBarLayout)

    def balanceWidget(self):
        balanceWidget = QWidget()
        balanceWidgetLayout = QGridLayout()
        balanceHeaderLabel = QLabel("Balance")
        balanceWidgetLayout.addWidget(balanceHeaderLabel, 0, 0)
        balanceWidgetLayout.addWidget(self.balanceLabel, 1, 0)
        balanceWidget.setLayout(balanceWidgetLayout)
        return balanceWidget

    def stakingWidget(self):
        stakingWidget = QWidget()
        stakingWidgetLayout = QGridLayout()
        stakingHeaderLabel = QLabel("Staking")
        stakingWidgetLayout.addWidget(stakingHeaderLabel, 0, 0)
        stakingWidgetLayout.addWidget(self.stakingLabel, 1, 0)
        stakingWidget.setLayout(stakingWidgetLayout)
        return stakingWidget

    def unconfirmedWidget(self):
        unconfirmedWidget = QWidget()
        unconfirmedWidgetLayout = QGridLayout()
        unconfirmedHeaderLabel = QLabel("Unconfirmed")
        unconfirmedWidgetLayout.addWidget(unconfirmedHeaderLabel, 0, 0)
        unconfirmedWidgetLayout.addWidget(self.unconfirmedLabel, 1, 0)
        unconfirmedWidget.setLayout(unconfirmedWidgetLayout)
        return unconfirmedWidget

    def totalWidget(self):
        totalWidget = QWidget()
        totalWidgetLayout = QGridLayout()
        totalHeaderLabel = QLabel("Total")
        totalWidgetLayout.addWidget(totalHeaderLabel, 0, 0)
        totalWidgetLayout.addWidget(self.totalLabel, 1, 0)
        totalWidget.setLayout(totalWidgetLayout)
        return totalWidget

    def balanceSpacerItem(self):
        balanceSpacerItem = QSpacerItem(1, 1, QSizePolicy.Expanding, QSizePolicy.Minimum)
        return balanceSpacerItem

    def getBalanceBar(self):
        return self.baseWidget

    def update(self):
        if self.parent.running == False:
            self.balanceLabel.setText("---")
            self.stakingLabel.setText("---")
            self.unconfirmedLabel.setText("---")
            self.totalLabel.setText("---")
        else:
            self.balanceLabel.setText(str(self.parent.num_balance))
            self.stakingLabel.setText(str(self.parent.num_staking))
            self.unconfirmedLabel.setText(str(self.parent.num_unconfirmed))
            self.totalLabel.setText(str(self.parent.num_total))

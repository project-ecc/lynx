#! /usr/bin/env python3

# Character Encoding: UTF-8

#system includes
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

#user includes
from slickrpc import Proxy
from slickrpc import exc

class SendPage(QObject):

    def __init__(self, parent):
        super(QObject, self).__init__()
        self.parent = parent

        self.baseWidget = QWidget()
        self.sendLayout = QVBoxLayout()
        self.sendAddressField = QLineEdit("Enter Address")
        self.sendAmountField = QLineEdit("Enter Amount")
        self.sendSubtractFeeOption = QCheckBox("Subtract tx fee from send amount")
        self.sendTransactionButton = QPushButton("Send")
        self.sendLayout.addWidget(self.sendAddressField)
        self.sendLayout.addWidget(self.sendAmountField)
        self.sendLayout.addWidget(self.sendSubtractFeeOption)
        self.sendLayout.addWidget(self.sendTransactionButton)
        self.baseWidget.setLayout(self.sendLayout)
        self.sendTransactionButton.clicked.connect(self.sendTransaction)

    def getBaseWidget(self):
        return self.baseWidget

    def sendTransaction(self):
        strAddr = '"' + self.sendAddressField.text() + '"'
        numAmount = self.sendAmountField.text()
        subtractFee = self.sendSubtractFeeOption.isChecked()
        try:
            res = self.parent.eccoind.sendtoaddress(strAddr, numAmount, "", "", subtractFee)
            self.parent.logAddText(res)
        except exc.RpcException as x:
            self.parent.logAddText(x.message)

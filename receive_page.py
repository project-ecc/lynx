#! /usr/bin/env python3

# Character Encoding: UTF-8

#system includes
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

#user includes
from slickrpc import Proxy
from slickrpc import exc

class ReceivePage(QObject):

    def __init__(self, parent):
        super(QObject, self).__init__()
        self.parent = parent

        self.baseWidget = QWidget()
        self.receivingLayout = QVBoxLayout()
        self.receivingWidget = QTableWidget()
        self.getNewAddressButton = QPushButton("Get New Address")
        self.receivingLayout.addWidget(self.getNewAddressButton)
        self.receivingLayout.addWidget(self.receivingWidget)
        self.baseWidget.setLayout(self.receivingLayout)
        self.getNewAddressButton.clicked.connect(self.getNewAddress)
        self.receivingAddrSet = set()
        self.buildReceivingWidget()

    def getBaseWidget(self):
        addrList = self.parent.eccoind.listaddresses()
        for addr in addrList:
            new = addr not in self.receivingAddrSet
            if new:
                self.receivingAddrSet.add(addr)
                self.updateReceivingWidget(addr)
        return self.baseWidget

    def updateReceivingWidget(self, addr):
        rowNum = self.receivingWidget.rowCount()
        self.receivingWidget.insertRow(rowNum)
        self.receivingWidget.setItem(rowNum, 0, QTableWidgetItem(str(addr)))

    def buildReceivingWidget(self):
        self.receivingWidget.setColumnCount(1)
        self.receivingWidget.setRowCount(0)
        self.receivingWidget.setHorizontalHeaderLabels(["Receiving Addresses"])
        self.receivingWidget.setEditTriggers(QTableWidget.NoEditTriggers)
        # makes the table take up the entire width of the qgridlayout slot
        self.receivingWidget.horizontalHeader().setStretchLastSection(True)

    def getNewAddress(self):
        newAddr = self.parent.eccoind.getnewaddress()
        self.receivingAddrSet.add(newAddr)
        self.updateReceivingWidget(newAddr)

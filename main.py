#! /usr/bin/env python3

# Character Encoding: UTF-8

#system includes
import os
import subprocess
import sys
import threading
import time
import zmq
import PyQt5
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
import sys

from PyQt5 import QtWidgets
from qt_material import apply_stylesheet

# user includes
import settings
from slickrpc import Proxy
from slickrpc import exc
import balance_bar
import console_page
import overview_page
import receive_page
import send_page
import tx_history_page

class MainWindow(QMainWindow):

    stopped = True
    starting = False
    running = False
    stopping = False
    eccoind = Proxy('http://%s:%s@%s' % (settings.rpc_user, settings.rpc_pass, settings.rpc_address))

    zmqPort = 28332
    zmqContext = zmq.Context()
    zmqSubSocket = zmqContext.socket(zmq.SUB)
    zmqSubSocket.setsockopt(zmq.SUBSCRIBE, b"hashblock")
    zmqSubSocket.setsockopt(zmq.SUBSCRIBE, b"hashtx")
    zmqSubSocket.setsockopt(zmq.SUBSCRIBE, b"system")
    zmqSubSocket.linger = 500
    zmqSubSocket.connect("tcp://127.0.0.1:%i" % zmqPort)
    zmqPoller = zmq.Poller()
    zmqPoller.register(zmqSubSocket, zmq.POLLIN)

    VERSION_MAJOR = 0
    VERSION_MINOR = 3
    VERSION_PATCH = 0

    str_bestBlockHash = ""
    num_headers = 0
    num_blocks = 0

    num_balance = 0.0
    num_staking = 0.0
    num_unconfirmed = 0.0
    num_total = 0.0

    def __init__(self):
        super(MainWindow, self).__init__()

        # first create widgets that might be affected by data retrieved from
        # the daemon status check and getAllInfo
        self.balanceBar = balance_bar.BalanceBar(self)
        self.textLog = QTextEdit()
        # sidebar buttons
        self.overviewButton = QPushButton()
        self.sendButton = QPushButton()
        self.receiveButton = QPushButton()
        self.transactionsButton = QPushButton()
        self.consoleButton = QPushButton()
        self.settingsButton = QPushButton()
        self.startstopButton = QPushButton()
        self.updateWalletButton = QPushButton()

        # set the buttons to disabled until we know the daemon status
        self.disableSideButtons()
        # check daemon status now because we might need the data
        try:
            self.checkDaemonStatus()
        except:
            pass

        # if running get some startup stats
        if self.running == True:
            self.getAllInfo()

        # run the zmq poller in a different thread
        self.zmqTimer = QTimer(self)
        self.zmqTimer.timeout.connect(self.zmqPollerCheck)
        self.zmqTimer.start(500)

        # overview widget
        self.overviewContent = overview_page.OverviewPage(self)
        # send widget and data
        self.sendContent = send_page.SendPage(self)
        # receiving widget and data
        self.receivingContent = receive_page.ReceivePage(self)
        # transaction history widget data
        self.transactionHistoryContent = tx_history_page.TxHistoryPage(self)
        # rpc console
        self.consoleContent = console_page.ConsolePage(self)
        # by default get the overview content widget
        self.contentWidget = self.overviewContent.getBaseWidget()

        self.mainWidget = QWidget()
        self.mainWidget.resize(1080, 720)
        # set a layout for the main widget
        self.mainLayout = QGridLayout()
        # add widgets to layout before setting layout
        self.mainLayout.addWidget(self.sidebar(), 0, 0, 3, 1)
        self.mainLayout.addWidget(self.balanceBar.getBalanceBar(), 0, 1, 1, 1)
        self.mainLayout.addWidget(self.contentWidget, 1, 1, 1, 1)
        self.mainLayout.addWidget(self.logBar(), 1, 2, 1, 1)
        self.mainLayout.setColumnStretch(0, 1);
        self.mainLayout.setColumnStretch(1, 10);
        self.mainLayout.setRowStretch(0, 1);
        self.mainLayout.setRowStretch(1, 10);
        self.mainWidget.setLayout(self.mainLayout)
        self.setWindowTitle("py.Lynx")
        self.setCentralWidget(self.mainWidget)
        # The show() method displays the widget on the screen.
        self.show()

    def checkDaemonStatus(self):
        res = self.eccoind.getinfo()
        if len(res) != 0:
            #daemon probably already running, adjust status accordingly
            self.stopped = False
            self.starting = False
            self.running = True
            self.stopping = False
            self.startStopUpdate()
            self.updateBalances()

    def processHashBlock(self, msg):
        str_msg = msg.hex()
        self.logAddText("New Best Block: " + str_msg + "\n")
        self.str_bestBlockHash = str_msg
        self.overviewContent.updateBestBlockHash(True)
        # if we get a block and we have some unconfirmed or staking balance,
        # use rpc to update the balances
        if self.num_staking != 0.0 or self.num_unconfirmed != 0:
            self.updateBalances()

    def processHashTx(self, msg):
        self.updateBalances()
        pass

    def processSystem(self, msg):
        lower_msg = msg[1:]
        self.logAddText(lower_msg + str("\n").encode('ascii'))
        if lower_msg == b"Startup: RPC available\n":
            self.starting = False
            self.running = True
            self.stopping = False
            self.stopped = False
            self.startStopUpdate()
            self.getAllInfo()
            self.overviewContent.updateAll()
            self.updateBalances()
        if lower_msg == b"Shutdown: done\n":
            self.starting = False
            self.running = False
            self.stopping = False
            self.stopped = True
            self.startStopUpdate()
            self.overviewContent.updateAll()

    def getAllInfo(self):
        info = self.eccoind.getinfo()
        self.num_blocks = info["blocks"]
        self.num_headers = info["headers"]
        bcinfo = self.eccoind.getblockchaininfo()
        self.str_bestBlockHash = bcinfo["bestblockhash"]
        walletinfo = self.eccoind.getwalletinfo()
        self.num_balance = walletinfo["balance"]
        self.num_unconfirmed = walletinfo["unconfirmed_balance"]
        self.num_staking = walletinfo["immature_balance"]
        self.num_total = self.num_balance + self.num_unconfirmed + self.num_staking

    def zmqPollerCheck(self):
        events = dict(self.zmqPoller.poll(100))
        if self.zmqSubSocket in events and events[self.zmqSubSocket] == zmq.POLLIN:
            msg = self.zmqSubSocket.recv_multipart()
            type = msg[0]
            body = msg[1]
            if type == b"hashblock":
                self.processHashBlock(body)
            elif type == b"hashtx":
                pass
            elif type == b"system":
                self.processSystem(body)

    def logAddText(self, text):
        strtext = ""
        if isinstance(text, bytes):
            strtext = text.decode('ascii')
        else:
            strtext = text
        self.textLog.append(strtext)

    def logBar(self):
        self.textLog.setReadOnly(True)
        return self.textLog

    def getOverviewButton(self):
        self.overviewButton.setText("Overview")
        self.overviewButton.clicked.connect(self.setOverviewWidget)
        return self.overviewButton


    def getSendButton(self):
        self.sendButton.setText("Send")
        self.sendButton.clicked.connect(self.setSendWidget)
        return self.sendButton


    def getReceiveButton(self):
        self.receiveButton.setText("Receive")
        self.receiveButton.clicked.connect(self.setReceivingWidget)
        return self.receiveButton


    def getTransactionsButton(self):
        self.transactionsButton.setText("Transactions")
        self.transactionsButton.clicked.connect(self.setTransactionHistoryWidget)
        return self.transactionsButton

    def getConsoleButton(self):
        self.consoleButton.setText("Console")
        self.consoleButton.clicked.connect(self.setConsoleWidget)
        return self.consoleButton

    def getSettingsButton(self):
        self.settingsButton.setText("Settings")
        return self.settingsButton

    def getEccoindPath(self):
        path = ""
        if sys.platform == "linux":
            #linux
            path = os.path.expanduser("~")
            path = path + "/.eccoin-wallet/"
            is_64bits = sys.maxsize > 2**32
            if is_64bits == True:
                path = path + "eccoind-linux64"
            else:
                path = path + "eccoind-linux32"
        elif sys.platform == "darwin":
            path = os.path.expanduser("~")
            path = path + "/Library/Application Support/.eccoin-wallet/"
            path = path + 'eccoind'
            # macOs
            pass
        elif sys.platform == "win32":
            #windows
            pass
        return path

    def walletStartStop(self):
        if self.stopped == True:
            eccoindPath = self.getEccoindPath()
            subprocess.run([eccoindPath, "-daemon", "-zmqpubhashtx=tcp://127.0.0.1:"+str(self.zmqPort), "-zmqpubhashblock=tcp://127.0.0.1:"+str(self.zmqPort), "-zmqpubsystem=tcp://127.0.0.1:"+str(self.zmqPort)])
            self.stopped = False
            self.starting = True
            self.running = False
            self.stopping = False
            self.startStopUpdate()
        elif self.running == True:
            self.eccoind.stop()
            self.starting = False
            self.running = False
            self.stopping = True
            self.stopped = False
            self.startStopUpdate()
        # otherwise do nothing

    def updateBalances(self):
        result = self.eccoind.getwalletinfo()
        self.num_balance = result["balance"]
        self.num_staking = result["immature_balance"]
        self.num_unconfirmed = result["unconfirmed_balance"]
        self.num_total = self.num_balance + self.num_staking + self.num_unconfirmed
        self.balanceBar.update()

    def enableSideButtons(self):
        self.overviewButton.setEnabled(True)
        self.sendButton.setEnabled(True)
        self.receiveButton.setEnabled(True)
        self.transactionsButton.setEnabled(True)

    def disableSideButtons(self):
        self.overviewButton.setEnabled(False)
        self.sendButton.setEnabled(False)
        self.receiveButton.setEnabled(False)
        self.transactionsButton.setEnabled(False)

    def startStopUpdate(self):
        self.disableSideButtons()
        if self.running == True:
            self.enableSideButtons()
            self.startstopButton.setText("Stop Wallet")
            return
        elif self.stopped == True:
            self.startstopButton.setText("Start Wallet")
        elif self.starting == True:
            self.startstopButton.setText("Starting Wallet...")
        elif self.stopping == True:
            self.startstopButton.setText("Stopping Wallet...")

    def getWalletButton(self):
        self.startStopUpdate()
        self.startstopButton.clicked.connect(self.walletStartStop)
        return self.startstopButton

    def getUpdateButton(self):
        self.updateWalletButton.setText("Update Wallet")
        return self.updateWalletButton

    def sidebar(self):
        sidebarWidget = QWidget()
        sidebarLayout = QVBoxLayout()

        LynxIcon = QPixmap("resources/lynx_logo.png")
        LynxLogo = QLabel();
        LynxLogo.setPixmap(LynxIcon.scaled(LynxLogo.size() / 2, Qt.KeepAspectRatio, Qt.SmoothTransformation))
        LynxLogo.setScaledContents(True)

        LogoSizePolicy = QSizePolicy()
        LogoSizePolicy.setHeightForWidth(True)
        LynxLogo.setSizePolicy(LogoSizePolicy)

        sidebarLayout.addWidget(LynxLogo)
        sidebarLayout.addWidget(self.getOverviewButton())
        sidebarLayout.addWidget(self.getSendButton())
        sidebarLayout.addWidget(self.getReceiveButton())
        sidebarLayout.addWidget(self.getTransactionsButton())
        sidebarLayout.addWidget(self.getSettingsButton())
        sidebarLayout.addWidget(self.getConsoleButton())
        sidebarLayout.addWidget(self.getWalletButton())
        sidebarLayout.addWidget(self.getUpdateButton())
        sidebarWidget.setLayout(sidebarLayout)
        return sidebarWidget

    def updateLog(newText):
        logLabel.setText(logLabel.text() + newText + "\n")

    def setSendWidget(self):
        self.setContentWidget(self.sendContent.getBaseWidget())

    def setContentWidget(self, content):
        # hide the old widget
        self.contentWidget.setVisible(False)
        self.mainLayout.replaceWidget(self.contentWidget, content)
        self.contentWidget = content
        # the current content widget might have been hidden in a previous
        # content switch, explicitly set it to visible for sanity
        self.contentWidget.setVisible(True)

    def setOverviewWidget(self):
        self.setContentWidget(self.overviewContent.getBaseWidget())

    def setReceivingWidget(self):
        self.setContentWidget(self.receivingContent.getBaseWidget())

    def setTransactionHistoryWidget(self):
        self.setContentWidget(self.transactionHistoryContent.getBaseWidget())

    def setConsoleWidget(self):
        self.setContentWidget(self.consoleContent.getBaseWidget())

def Main():
    # enforce python3 requirement
    if sys.version_info[0] < 3:
        raise 'Use Python 3.7+'
    if sys.version_info[1] < 7:
        raise 'Use Python 3.7+'

    # start the gui
    # Every PyQt5 application must create an application object.
    # The application object is located in the QtWidgets module.
    app = QApplication(sys.argv)

    mainWindow = MainWindow()

    apply_stylesheet(app, theme='dark_amber.xml')
    mainWindow.show()

    # Finally, we enter the mainloop of the application.
    app.exec()

if __name__ == '__main__':
   Main()

#! /usr/bin/env python3

# Character Encoding: UTF-8

#system includes
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

#user includes
from slickrpc import Proxy
from slickrpc import exc

class OverviewPage(QObject):

    def __init__(self, parent):
        super(QObject, self).__init__()
        self.parent = parent

        self.baseWidget = QWidget()
        self.overviewLayout = QVBoxLayout()
        self.statusLabel = QLabel("Wallet Status: Off")
        self.numHeadersLabel = QLabel("NaN")
        self.numBlocksLabel = QLabel("NaN")
        self.bestBlockHashLabel = QLabel("NaN")
        self.overviewLayout.addWidget(self.statusLabel)
        self.overviewLayout.addWidget(self.numHeadersLabel)
        self.overviewLayout.addWidget(self.numBlocksLabel)
        self.overviewLayout.addWidget(self.bestBlockHashLabel)
        self.baseWidget.setLayout(self.overviewLayout)

    def getBaseWidget(self):
        self.updateAll()
        return self.baseWidget

    def updateStatus(self):
        prefix = "Wallet Status: "
        suffix = "Off"
        res = False
        if self.parent.stopped == True:
            # base case, intentionally do nothing
            pass
        elif self.parent.starting == True:
            suffix = "Starting"
        elif self.parent.running == True:
            suffix = "Running"
            res = True
        elif self.parent.stopping == True:
            suffix = "Stopping"
        self.statusLabel.setText(prefix + suffix)
        return res

    def updateNumHeaders(self, running):
        if running == True:
            self.numHeadersLabel.setText("Headers Synced: " + str(self.parent.num_headers))
        else:
            self.numHeadersLabel.setText("Headers Synced: NaN")

    def updateNumBlocks(self, running):
        if running == True:
            self.numBlocksLabel.setText("Blocks Synced: " + str(self.parent.num_blocks))
        else:
            self.numBlocksLabel.setText("Blocks Synced: NaN")

    def updateBestBlockHash(self, running):
        if running == True:
            self.bestBlockHashLabel.setText("Best Block Hash: " + str(self.parent.str_bestBlockHash))
        else:
            self.bestBlockHashLabel.setText("Best Block Hash: NaN")

    def updateAll(self):
        running = self.updateStatus()
        self.updateNumHeaders(running)
        self.updateNumBlocks(running)
        self.updateBestBlockHash(running)

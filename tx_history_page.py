#! /usr/bin/env python3

# Character Encoding: UTF-8

#system includes
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

#user includes
from slickrpc import Proxy
from slickrpc import exc

class TxHistoryPage(QObject):

    def __init__(self, parent):
        super(QObject, self).__init__()
        self.parent = parent

        self.baseWidget = QWidget()
        self.transactionHistoryLayout = QVBoxLayout()
        self.transactionHistoryWidget = QTableWidget()
        self.loadMoreTransactionsButton = QPushButton("Load 500 more")
        self.transactionHistoryLayout.addWidget(self.transactionHistoryWidget)
        self.transactionHistoryLayout.addWidget(self.loadMoreTransactionsButton)
        self.baseWidget.setLayout(self.transactionHistoryLayout)
        self.transactionHistoryDepth = 0
        self.loadMoreTransactionsButton.clicked.connect(self.loadMoreTransactionHistory)
        self.buildTransactionHistoryWidget()

    def getBaseWidget(self):
        if self.transactionHistoryDepth == 0:
            recentTransactions = self.parent.eccoind.listtransactions(500)
            if len(recentTransactions) < 500:
                self.loadMoreTransactionsButton.setEnabled(False)
            for tx in recentTransactions:
                self.updateTransactionHistoryWidget(tx, True)
        self.transactionHistoryWidget.horizontalHeader().resizeSections(QHeaderView.ResizeToContents)
        return self.baseWidget

    def updateTransactionHistoryWidget(self, tx, placeTop):
        rowNum = 0
        if placeTop:
            rowNum = 0
        else:
            rowNum = self.transactionHistoryWidget.rowCount()
        self.transactionHistoryWidget.insertRow(rowNum)
        self.transactionHistoryWidget.setItem(rowNum, 0, QTableWidgetItem(str(tx["confirmations"])))
        self.transactionHistoryWidget.setItem(rowNum, 1, QTableWidgetItem(str(tx["category"])))
        self.transactionHistoryWidget.setItem(rowNum, 2, QTableWidgetItem(str(tx["address"])))
        self.transactionHistoryWidget.setItem(rowNum, 3, QTableWidgetItem(str(tx["amount"])))
        self.transactionHistoryWidget.setItem(rowNum, 4, QTableWidgetItem(str(tx["txid"])))
        self.transactionHistoryDepth = self.transactionHistoryDepth + 1

    def loadMoreTransactionHistory(self):
        recentTransactions = self.parent.eccoind.listtransactions(500, self.transactionHistoryDepth)
        for tx in recentTransactions:
            self.updateTransactionHistoryWidget(tx, False)
        self.transactionHistoryWidget.horizontalHeader().resizeSections(QHeaderView.ResizeToContents)

    def buildTransactionHistoryWidget(self):
        # confirmations, category, address, amount, txid
        self.transactionHistoryWidget.setColumnCount(5)
        self.transactionHistoryWidget.setRowCount(0)
        self.transactionHistoryWidget.setHorizontalHeaderLabels(["Confirmations", "Category", "Address", "Amount", "Txid"])
        self.transactionHistoryWidget.setEditTriggers(QTableWidget.NoEditTriggers)
        self.transactionHistoryWidget.horizontalHeader().setStretchLastSection(True)

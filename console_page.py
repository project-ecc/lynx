#! /usr/bin/env python3

# Character Encoding: UTF-8

#system includes
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

#user includes
from slickrpc import Proxy
from slickrpc import exc

import ujson

class ConsolePage(QObject):

    def __init__(self, parent):
        super(QObject, self).__init__()
        self.parent = parent

        self.baseWidget = QWidget()
        self.consoleLayout = QVBoxLayout()
        self.consoleLog = QTextEdit()
        self.consoleLog.setReadOnly(True)
        self.consoleInput = QLineEdit()
        self.consoleInput.returnPressed.connect(self.submitCommand)
        self.consoleLayout.addWidget(self.consoleLog)
        self.consoleLayout.addWidget(self.consoleInput)
        self.baseWidget.setLayout(self.consoleLayout)


    def getBaseWidget(self):
        return self.baseWidget

    def logAddText(self, text):
        strtext = ""
        if isinstance(text, bytes):
            strtext = text.decode('ascii')
        else:
            strtext = text
        self.consoleLog.append(strtext)

    def submitCommand(self):
        text = self.consoleInput.text()
        self.consoleInput.clear()
        self.logAddText("> " + text)
        try:
            res = getattr(self.parent.eccoind, text)()
            if isinstance(res, str):
                self.logAddText(res + "\n\n")
            else:
                self.logAddText(ujson.dumps(res, indent=4) + "\n\n")
        except exc.RpcException as x:
            self.logAddText(x.message + "\n\n")
